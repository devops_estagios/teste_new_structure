
  CREATE OR REPLACE FUNCTION AAF_GET_DOTNET_PARAMS (token in varchar2) return varchar2 is
--Esta fun��o � usada para passar os par�metros do Processo Cl�nico .Net para
--as aplica��es em Oracle Forms.
--O token que a fun��o recebe como par�metro � passado no URL que invoca o form (em ambiente web);
--Na Template, o form chama esta fun��o ghf_get_dotnet_params com o token que recebe.
--Esta fun��o devolve uma string com os valores de v�rias vari�veis globais, no formato seguinte:
--
--  [globalName1]=[value1]#[globalName2]=[value2]#
--
--Exemplo (de valor de retorno da fun��o):
--  global.cg$t_doente=HS#global.cg$doente=100#

	cursor cursor_params (p_token varchar2) is
		select key, value
		from aa_app_token a, aa_app_token_detail b
		where a.app_token_id = b.app_token_id and
      		  token = p_token;

    return_str varchar2(2000) := '';
    globalName varchar2(100) := '';


    --Devolve o utilizador_id associado ao n_mecan indicado
    function get_utilizador_id(p_n_mecan in varchar2) return varchar2 is
        w_utilizador_id varchar2(100) := '';
    begin
	   /*select utilizador_id
	   into w_utilizador_id
	   from utilizador
	   where emp_num = p_n_mecan;*/

	   select user_sys
	   into w_utilizador_id
	   from sd_pess_hosp_def
	   where n_mecan = p_n_mecan;

	   return w_utilizador_id;
	exception when others then
       return '';
	end;



begin

    for i in cursor_params(token) loop
      	if (lower(i.key) = 't_doente') then
            globalName := 'global.cg$t_doente';
      	elsif (lower(i.key) = 'doente') then
            globalName := 'global.cg$doente';
      	elsif (lower(i.key) = 't_episodio') then
            globalName := 'global.cg$t_episodio';
      	elsif (lower(i.key) = 'episodio') then
            globalName := 'global.cg$episodio';
      	elsif (lower(i.key) = 'n_mecan') then
            --Aqui � necess�rio preencher tamb�m a global.utilizador_id
            return_str := return_str||'global.utilizador_id='||get_utilizador_id(i.value)||'#';
            globalName := 'global.cg$n_mecan';
 		elsif (lower(i.key) = 'cod_serv') then
            globalName := 'global.cg$cod_serv';
 		elsif (lower(i.key) = 'aplicacao_id') then
            globalName := 'global.aplicacao_id';
 		elsif (lower(i.key) = 'assoc_type') then
            globalName := 'global.param$tipo_assoc';
 		end if;

        return_str := return_str||globalName||'='||i.value||'#';
    end loop;

    return return_str;
end;/